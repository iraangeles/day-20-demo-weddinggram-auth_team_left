/**
 * Created by phangty on 10/11/16.
 */
(function () {
    angular
        .module("weddingGramApp")
        .controller("LoginCtrl", ["$state", "$stateParams", "AuthFactory", "Flash", "UserAPI", "ngProgressFactory", LoginCtrl]);

    function LoginCtrl($state, $stateParams, AuthFactory, Flash,UserAPI, ngProgressFactory){
        var vm = this;


        // console.log ("user login---" + $stateParams.token);
        
        var params = $stateParams.token;


        
        if (params){
            console.log("params")
            UserAPI
                .verifyEmailSvc(params)
                .then(function (){
                    console.log("done");
                    $state.go("verifyemail");
                })
                .catch(function(){
                    console.log("error");
 
                })
        }






        vm.progressbar = ngProgressFactory.createInstance();

        vm.login = function () {
            vm.progressbar.start();
            AuthFactory.login(vm.user)
                .then(function () {
                    if(AuthFactory.isLoggedIn()){
                        vm.emailAddress = "";
                        vm.password = "";
                        vm.progressbar.complete();
                        $state.go("weddinggram");
                    }else{
                        Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        vm.progressbar.stop();
                        $state.go("SignIn");
                    }
                }).catch(function () {
                    vm.progressbar.stop();
                    console.error("Error logging on !");
                });
        };
    }
})();